open Core

module Fabric = struct
  type t = { nr: int
           ; x : int
           ; y : int
           ; width : int
           ; height : int
           ; left : int
           ; right : int
           ; top : int
           ; bottom : int
           }

  let fabric_regex =
    Str.regexp "#\\([0-9]+\\) @ \\([0-9]+\\),\\([0-9]+\\): \\([0-9]+\\)x\\([0-9]+\\)"

  let to_string fabric =
    "Fabric (#" ^ string_of_int fabric.nr
    ^ " @ " ^ string_of_int fabric.x
    ^ "," ^ string_of_int fabric.y
    ^ ": " ^ string_of_int fabric.width
    ^ "x" ^ string_of_int fabric.height
    ^ ")"

  let of_string line =
    match Str.string_match fabric_regex line 0 with
    | false -> None
    | true ->
      let nr = Int.of_string @@ Str.matched_group 1 line in
      let x = Int.of_string @@ Str.matched_group 2 line in
      let y = Int.of_string @@ Str.matched_group 3 line in
      let width = Int.of_string @@ Str.matched_group 4 line in
      let height = Int.of_string @@ Str.matched_group 5 line in
      Some ({nr = nr
            ; x = x
            ; y = y
            ; width = width
            ; height = height
            ; left = x
            ; right = x + width
            ; top = y
            ; bottom = y + height
            })

  let overlap f1 f2 =
    let x_overlap = (max 0 (min (f1.right) (f2.right) - (max f1.left f2.left))) in
    let y_overlap = (max 0 (min (f1.bottom) (f2.bottom) - (max f1.top f2.top))) in
    let overlapArea = x_overlap * y_overlap in
    overlapArea > 0

  let overlap_coords f1 f2 =
    let right_diff = min f1.right f2.right in
    let left_diff = max f1.left f2.left in
    let bottom_diff = min f1.bottom f2.bottom in
    let top_diff = max f1.top f2.top in
    if (left_diff < right_diff && top_diff < bottom_diff) then
      let coords = List.cartesian_product (List.range left_diff right_diff) (List.range top_diff bottom_diff) in
      coords
    else
      []
end

let claims =
  [ "#1 @ 1,3: 4x4"
  ; "#2 @ 3,1: 4x4"
  ; "#3 @ 5,5: 2x2"
  ]

module PairSet = Set.Make(
  struct
    let compare = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
    let sexp_of_t = fun p -> Tuple2.sexp_of_t sexp_of_int sexp_of_int p
    let t_of_sexp = fun p ->
      Tuple2.t_of_sexp int_of_sexp int_of_sexp p
    type t = (int, int) Tuple2.t
  end)

let () =
  let lines = In_channel.input_lines (In_channel.create "data.txt") in
  let fabrics = List.filter_map ~f:Fabric.of_string lines in
  let overall_area = List.foldi ~init:PairSet.empty ~f:(fun i acc f ->
      PairSet.union acc
      ( List.fold ~init:PairSet.empty ~f:(fun otheracc other ->
            PairSet.union otheracc ( PairSet.of_list @@ Fabric.overlap_coords f other )
          ) @@ List.drop fabrics (i + 1) )
    ) fabrics
  in
  print_endline ("There are " ^ string_of_int ( PairSet.length overall_area ) ^ " overlapping squares" )

let () =
  let lines = In_channel.input_lines (In_channel.create "data.txt") in
  let fabrics = List.filter_map ~f:Fabric.of_string lines in
  print_endline ("There are " ^ string_of_int (List.length fabrics) ^ " fabrics");
  List.iter fabrics ~f:(fun f ->
      let overlaps = List.filter fabrics ~f:(fun o ->
          Fabric.overlap f o
        )
      in
      if (List.length overlaps = 1) then
        print_endline (Fabric.to_string f ^ " has no overlaps ")
      (* else *)
        (* print_endline (Fabric.to_string f ^ " has overlaps " ^ List.to_string ~f:Fabric.to_string overlaps) *)
      (* else *)
        (* print_endline (Fabric.to_string f ^ " has " ^ string_of_int (List.length overlaps) ^ " overlaps") *)
    )
