open Core

type charFrequencyMap = int Char.Map.t

let print_map map =
  print_endline "{";
  Char.Map.iteri map
    ~f:(fun ~key:key ~data:value ->
        print_endline ("  " ^ Char.to_string key ^ ": " ^ string_of_int value));
  print_endline "}"

let frequencies word =
  let increment_frequency char_opt =
    char_opt
    |> Option.map ~f:(fun i -> i + 1)
    |> Option.value ~default:1
  in

  let all_counts = List.fold_left
      (String.to_list word)
      ~f:(fun freqs char -> Char.Map.update freqs char ~f:increment_frequency)
      ~init:Char.Map.empty
  in

  (* print_map all_counts; *)

  Char.Map.filter all_counts ~f:(fun count -> count = 2 || count = 3)

let value_of_word word =
  let freqs = frequencies word in
  ( Char.Map.exists ~f:(fun f -> f = 2) freqs
  , Char.Map.exists ~f:(fun f -> f = 3) freqs
  )

let checksum words =
  let doubles_and_triples = List.map ~f:value_of_word words in
  (* List.iter doubles_and_triples
   *   ~f:(fun (doubles, triples) -> print_endline ("doubles: " ^ string_of_bool doubles ^ " triples: " ^ string_of_bool triples)); *)
  let number_of_doubles_and_triples =
    List.fold_left ~f:(fun (double_nr, triple_nr) (has_double, has_triple) ->
        match (has_double, has_triple) with
        | (true, true) -> (double_nr + 1, triple_nr + 1)
        | (true, false) -> (double_nr + 1, triple_nr)
        | (false, true) -> (double_nr, triple_nr + 1)
        | _ -> (double_nr, triple_nr)
      )
      ~init:(0, 0)
      doubles_and_triples
  in
  Tuple2.uncurry (fun a b -> a * b) number_of_doubles_and_triples

let loevenstein word_a word_b =
  let rec go distance a b =
    match (a, b) with
    | ([], _) -> distance
    | (_, []) -> distance
    | (fa :: ra, fb :: rb) ->
      if fa = fb then
        go distance ra rb
      else
        go (distance + 1) ra rb
  in
  go 0 (String.to_list word_a) (String.to_list word_b)

let find_with_distance_1 word words =
  List.find_map
    ~f:(fun cur_word -> if loevenstein word cur_word = 1 then Some cur_word else None)
    words

let pair_with_loevenstein_distance_1 words =
  List.find_map
    ~f:(fun cur_word ->
        match (find_with_distance_1 cur_word words) with
        | Some m -> Some (cur_word, m)
        | None -> None
      )
    words

let common_letters word_a word_b =
  let rec go acc wa wb =
    match (wa, wb) with
    | ([], _) -> acc
    | (_, []) -> acc
    | (fa :: ra, fb :: rb) ->
      if (fa = fb) then
        go (acc ^ String.of_char fa) ra rb
      else
        go acc ra rb
  in
  go "" (String.to_list word_a) (String.to_list word_b)
