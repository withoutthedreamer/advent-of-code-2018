open OUnit2
open Core

let abcdef_has_not_duplicate_or_triples _ =
  Lib.frequencies "abcdef"
  |> Char.Map.is_empty
  |> assert_bool "The frequency map is not empty"

let bababc_contains_two_a_and_three_b _ =
  let freqs = Lib.frequencies "bababc" in
  (* Lib.print_map freqs; *)
  let number_of_as = Char.Map.find_exn freqs 'a' in
  let number_of_bs = Char.Map.find_exn freqs 'b' in
  assert_equal
    ~msg:"should have two a's"
    number_of_as 2;
  assert_equal
    ~msg:"should have three b's"
    number_of_bs 3

let abbcde_contains_two_bs _ =
  let freqs = Lib.frequencies "abbcde" in
  assert_equal
    ( Char.Map.find freqs 'b' )
    ( Option.some 2 )

let checksum_should_multiply_the_duplicates_and_triplets _ =
  let checksum = Lib.checksum
      [ "abcdef"
      ; "bababc"
      ; "abbcde"
      ; "abcccd"
      ; "aabcdd"
      ; "abcdee"
      ; "ababab"
      ]
  in
  assert_equal
    ~printer:string_of_int
    12
    checksum

let print_tuple tuple =
  Tuple2.uncurry
    (fun doubles triples -> "(" ^ string_of_bool doubles ^ ", " ^ string_of_bool triples ^ ")")
    tuple

let value_of_word_should_return_if_it_contains_doubles_and_triples _ =
  assert_equal
    ~printer:print_tuple
    (false, false) (Lib.value_of_word "abcdef");
  assert_equal
    ~printer:print_tuple
    (true, true) (Lib.value_of_word "bababc");
  assert_equal
    ~printer:print_tuple
    (true, false) (Lib.value_of_word "abbcde");
  assert_equal
    ~printer:print_tuple
    (false, true) (Lib.value_of_word "abcccd");
  assert_equal
    ~printer:print_tuple
    (true, false) (Lib.value_of_word "aabcdd");
  assert_equal
    ~printer:print_tuple
    (false, true) (Lib.value_of_word "ababab")

let loevenstein_distance _ =
  assert_equal
    ~printer:string_of_int
    0 (Lib.loevenstein "a" "a");
  assert_equal
    ~printer:string_of_int
    1 (Lib.loevenstein "a" "b");
  assert_equal
    ~printer:string_of_int
    2 (Lib.loevenstein "abcde" "axcye");
  assert_equal
    ~printer:string_of_int
    1 (Lib.loevenstein "fghij" "fguij")

let find_words_of_loevenstein_distance_1 _ =
  let words =
    [ "abcde"
    ; "fghij"
    ; "klmno"
    ; "pqrst"
    ; "fguij"
    ; "axcye"
    ; "wvxyz"
    ]
  in
  assert_equal
    ~printer:(fun (word_a, word_b) -> "(" ^ word_a ^ ", " ^ word_b ^ ")")
    ("fghij", "fguij")
    ( Option.value_exn ( Lib.pair_with_loevenstein_distance_1 words ) )

let common_letters _ =
  assert_equal
    ~printer:ident
    "fgij" (Lib.common_letters "fghij" "fguij")

let suite =
  "suit" >:::
  [ "abcdef has no duplicates or triples" >:: abcdef_has_not_duplicate_or_triples
  ; "bababc contains two a's and three b's" >:: bababc_contains_two_a_and_three_b
  ; "abbcde contains two b's" >:: abbcde_contains_two_bs
  ; "value_of_word should return if it contains doubles and triples"
    >:: value_of_word_should_return_if_it_contains_doubles_and_triples
  ; "checksum should multiple doubles and triplets" >:: checksum_should_multiply_the_duplicates_and_triplets
  ; "loevenstein" >:: loevenstein_distance
  ; "loevenstein pairs with distance 1" >:: find_words_of_loevenstein_distance_1
  ; "common letters" >:: common_letters
  ]

let () =
  run_test_tt_main suite
