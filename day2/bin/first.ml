open Core

let () =
  let lines = In_channel.input_lines (In_channel.create "data.txt") in
  let cs = Lib.checksum lines in
  print_endline ( "Checksum is " ^ string_of_int cs)
