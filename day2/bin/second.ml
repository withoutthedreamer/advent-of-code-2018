open Core

let () =
  let lines = In_channel.input_lines @@ In_channel.create "data.txt" in
  let pair = Lib.pair_with_loevenstein_distance_1 lines in
  match pair with
  | Some (first, second) ->
    let common = Lib.common_letters first second in
    print_endline ("Pair is " ^ first ^ " and " ^ second ^ ". They have in common " ^ common)
  | None ->
    print_endline "No pair found"
