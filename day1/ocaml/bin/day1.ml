open Core

module IntSet = Set.Make(Int)

let evaluate_line line running_sum frequency_set =
  let value = int_of_string line in
  let new_running_sum = running_sum + value in
  if IntSet.mem frequency_set new_running_sum then
    Either.first new_running_sum
  else
    Either.second @@ (new_running_sum, IntSet.add frequency_set new_running_sum)

let rec evaluate_lines lines running_sum frequency_set =
  match lines with
      | [] -> Either.second (running_sum, frequency_set)
      | x :: xs ->
        let value = evaluate_line x running_sum frequency_set in
        match value with
            | First v -> Either.first v
            | Second (new_running_sum, set) -> evaluate_lines xs new_running_sum set

let rec frequency_searcher file_name running_sum frequency_set =
  let channel = In_channel.create file_name in
  let lines = In_channel.input_lines channel in
  match evaluate_lines lines running_sum frequency_set with
  | First v ->
    print_endline ("The first duplicate frequency is " ^ (string_of_int v))
  | Second (new_running_sum, set) -> frequency_searcher file_name new_running_sum set

let () =
  frequency_searcher "data.txt" 0 IntSet.empty
